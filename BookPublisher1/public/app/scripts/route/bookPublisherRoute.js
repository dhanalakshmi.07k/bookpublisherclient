bookPublisherApp.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){
  $routeProvider
    .when('/view1',{
      controller:'bookCtrlr',
      templateUrl:'view/test/view1.html'
    })
    .when('/bookDetails/:bookId',{
      controller:'bookDetailsById',
      templateUrl:'view/view3.html'
    })
    .when('/view3',{
      controller:'bookCtrlr',
      templateUrl:'view/view3.html'
    })
    .when('/slider',{
      controller:'bookCtrlr',
      templateUrl:'view/test/slider2.html'
    })
  /*  .when('/bookList',{
      templateUrl:'view/bookList.html'
    })*/
    .when('/aboutUs',{
          templateUrl:'view/aboutUs.html'
    })
    .when('/contactUs',{
    /*  controller:'bookCtrlr',*/
      templateUrl:'view/contactUs.html'
    })
    .when('/form',{
      controller:'bookCtrlr',
      templateUrl:'view/test/form.html'
    })
    .when('/test',{
      controller:'bookCtrlr',
      templateUrl:'view/test/sliderTest.html'
    })
    .when('/messageSentSuccess',{
          templateUrl:'view/messageSent.html'
     })
    .when('/',{
      /* controller:'bookCtrlr',*/
      templateUrl:'view/testBookList.html'
    })
    .when('/all/books',{
      templateUrl:'view/allBooks.html'
    })
    .when('/category/:mainCategoryName', {
      controller:'bookCtrlr',
      templateUrl: 'view/mainCategoryBooks.html'
    })
    .when('/category/:MainCategoryName/:subCategoryName', {
      controller:'bookCtrlr',
      templateUrl: 'view/categoryWiseBooks.html'
    })
    .otherwise({redirectTo:'/'});
  $/*locationProvider.html5Mode(true);*/
  $locationProvider.hashPrefix('!');
}]);
