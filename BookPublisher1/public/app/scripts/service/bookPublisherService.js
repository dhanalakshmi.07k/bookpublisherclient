bookPublisherApp.factory('bookService',function($http,$q){
  var bookService = {};
  var s3ImagePath="https://s3-ap-southeast-1.amazonaws.com/lucent-book-image/images/";
  var getBookDetailsBySearchCriteria = function(searchName,skip,limit){
    return $http.get("/search/book/name/"+searchName+"/"+skip+"/"+limit)
  }
  var getCountOfSearchedBookDetails = function(searchName){
    return $http.get("/search/book/count/name/"+searchName)
  }
  var getBooksByMainCategory =function(mainCatName){
    return $http.get("/books/category/"+mainCatName)
  }
  return {
    /* getAllBooks: function(){
     return $http.get("http://localhost:7800/all/books");
     },*/
    getMainCategoryForMetaDetails:function(){
      return  $http.get("/main/category/categoryName/");
    },
    getAllBooksByCategory:function(category){
      return $http.get("category/"+category);
    },
    getBookByBookId:function(bookId){
      return $http.get("book/bookId/"+bookId);
    },
    getAllBooksByAuthor:function(author){
      return $http.get("author/"+author);
    },
    getAllBooksByLimit: function(limit){
      return $http.get("all/limit/"+limit);
    },
    getAllCategories: function(){
      return $http.get("all/category/");
    },
    getTotalBooksCount: function(){
      return $http.get("total/book/count");
    },
    getAllAuthors: function(){
      return $http.get("/all/author/");
    },
    getBooksByrangeForPage: function(start,end){
      return $http.get("/all/books/"+start+"/"+end);
    },
    getMainCategories: function(){
      return $http.get("/main/category");
    },
    getNewBooks: function(){
      return $http.get("/all/books/new");
    },
    getTotalBooksCountByCategory: function(cName){
      return $http.get("/total/book/category/"+cName+"/count");
    },
    getTotalBooksCountByAuthor: function(aName){
      return $http.get("/total/book/author/"+aName+"/count");
    },
    getBooksByCategoryNameAndRangeForPage: function(cName,start,end){
      return $http.get("/all/books/category/"+cName+"/range/"+start+"/"+end);
    },
    getBooksByAuthorNameAndRangeForPage: function(aName,start,end){
      return $http.get("all/books/author/"+aName+"/range/"+start+"/"+end);
    },
    /*getBooksByLimit1: function(start,end){
     return $http.get("http://localhost:7800/all/books/range/"+start+"/"+end);
     },*/
    getBooksByLimit1: function(start,end){
      return $http.get("all/books/"+start+"/"+end);
    },
    addUserDetails:function(){
      return $http.post("http://192.168.1.108:8000/addUserQuries");
    },
    getAllSubCategoriesForCategory:function(cname){
      return $http.get("/category/mainCategory/"+cname);
    },
    getAllRelatedBooks:function(cateArray){
      return $http.get("/books/category/categories/"+cateArray);
    },
    getSearchBooks:function(){
      return $http.get("/search");
    },
    getS3ImagePath:function(){
      return  s3ImagePath;
    },
    bookByBookTitle:function(){
      return $http.get("/book/title/");
    },
    getBookDetailsBySearchCriteria:getBookDetailsBySearchCriteria,
    getCountOfSearchedBookDetails:getCountOfSearchedBookDetails,
    getBooksByMainCategory:getBooksByMainCategory
  }
});

