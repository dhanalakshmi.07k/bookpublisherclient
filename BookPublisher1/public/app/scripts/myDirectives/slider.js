/*var sliderApp = angular.module('sliderApp', []);*/

bookPublisherApp.directive('slider', function($timeout) {
  return {
    restrict: 'AE',
    replace: true,
    scope: {
      images: '='
    },
    link: function(scope, elem, attrs) {},
    templateUrl: 'view/test/slider.html'
  };
});
