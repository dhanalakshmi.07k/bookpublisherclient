var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

dbHost=process.env.DB_HOST_NAME;
dbPort=process.env.DB_PORT;

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'BookPublisher'
    },
    port: 7800,
    db:'mongodb://localhost/bookish',
    imageBaseUrl:' https://s3.amazonaws.com/dev-image-folder/images/',
  },

  test: {
    root: rootPath,
    app: {
      name: 'BookPublisher'
    },
    port: 7800,
    db: 'mongodb://localhost/bookish-test',
    imageBaseUrl:'https://s3-ap-southeast-1.amazonaws.com/lucent-book-image/images/'
  },
  production: {
    root: rootPath,
    app: {
      name: 'BookPublisher'
    },
    port: 80,
    db: 'mongodb://development:devPassw0rd@ds051990.mongolab.com:51990/bookish',
    imageBaseUrl:'https://s3-ap-southeast-1.amazonaws.com/lucent-book-image/images/'
  },
  docker: {
    root: rootPath,
    app: {
      name: 'BookPublisher'
    },
   db:'mongodb://'+dbHost+':'+dbPort+'/bookish',
    port: 7800,
    imageBaseUrl:' https://s3.amazonaws.com/dev-image-folder/images/',
  }
};

module.exports = config[env];
