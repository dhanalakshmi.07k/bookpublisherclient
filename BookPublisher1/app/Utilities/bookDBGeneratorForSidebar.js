var mongoose = require('mongoose');
var stdio = require('stdio');

/*mongoose.connect('mongodb://localhost/book_publisher');*/
mongoose.connect('mongodb://localhost/bookish');
var bookSchema = new mongoose.Schema(
  {
    bookId : Number,
    IsbnNumber10 : Number,
    IsbnNumber13 : Number,
    Title : String,
    ShortDesc : String,
    LongDesc : String,
    PublishedDate : Date,
    Language : String,
    Category : {
      names : [
        String
      ]
    },
    PageNo: Number,
    Author: {
      names: [
        String
      ]
    },
    Format: {
      format: [
        String
      ]
    },
    thumbnail : String,
    Images: {
      thumbnail: String,
      backcover: String,
      Preview: {
        Previews: [
          String
        ]
      }
    },
    Series: String,
    Edition: Number,
    PublisherRating: Number,
    copyRight: Number,
    PhysicalProperties: {
      dimension: {height:Number,width:Number},
      weight: Number
    },
    price: Number
  },{collection: "bookDetailsSidebar"});

var categorySchema = new mongoose.Schema(
  {
    categoryId : Number,
    Name : String,
    isMainCategory:Number,
    maincategoryName:String,
    Description : String,
    isFrequentlyUsed:Number
  },{collection: "bookCategoriesSidebar"});

var authorSchema = new mongoose.Schema(
  {
    authorId : Number,
    Description : String,
    name: {
      fullName: {
        firstName : String,
        lastName : String
      }
    },
    address: {
      fullAddress: {
        city : String,
        state: String,
        Street: String,
        ZIpCode : Number,
        Country : String
      }
    }},{collection: "bookAuthorsSidebar"});

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
var Books =  mongoose.model('books',bookSchema);
var Categories =  mongoose.model('categories',categorySchema);
var Authors =  mongoose.model('author',authorSchema);
/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Ends++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

var createDocument = function(noOfBooks){
  var data = noOfBooks;
  var Categories = ["Syllabus","Technical","General_Knowledge","Finance","Mathematics","Competetive","Syllabus1","Technical1","General_Knowledge1","Finance1","Mathematics1","Competetive1"
  ,"Syllabus2","Technical2","General_Knowledge2","Finance2","Mathematics2","Competetive2",
  ,"Syllabus3","Technical3","General_Knowledge3","Finance3","Mathematics3","Competetive3",
  ,"Syllabus4","Technical4","General_Knowledge4","Finance4","Mathematics4","Competetive4"];
  var BookTitle = ["Quantitative","Mathematics","English Vocabulary","Computer","Marketing","Finance"];
  var Author = ["Anand","Bharath","Ajit","Michael","Christopher","Shankara","Madhwa"];
  var Language = ["Kannada","Hindi","Tamil","Telugu","English"];
  var CopyRight = [1991,1992,2000,2001,2002,2003,2004,2005,2006,2007,2007,2008,2009,2010,2011,2012,2013,2014,2015];
  var numbers = [1,2];
  from = new Date(1900, 0, 1).getTime();
  to = new Date(2020, 0, 1).getTime();
  for(var i=1;i<data;i++){
    var book = new Books;
    var noOfCategories = numbers[Math.floor(Math.random()*numbers.length)];
    var noOfAuthors = numbers[Math.floor(Math.random()*numbers.length)];
    book.bookId = i;
    book.IsbnNumber10 = Math.floor(1000000000 + Math.random() * 900000000);
    book.IsbnNumber13 = Math.floor(1000000000000 + Math.random() * 900000000);
    book.Title= BookTitle[Math.floor(Math.random()*BookTitle.length)]+i;
    book.ShortDesc = "This is a goood book ,you will defnitely love it";
    book.LongDesc = "This is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love it";
    book.PublishedDate = new Date(from + Math.random() * (to - from));
    book.Language = Language[Math.floor(Math.random()*Language.length)];
    for(var j=0;j<=2;j++){
      book.Category.names[j]= Categories[Math.floor(Math.random()*Categories.length)]+1;
    }
    for(var k=0;k<=2;k++){
      book.Author.names[k] = Author[Math.floor(Math.random()*Author.length)];
    }
    book.PageNo = Math.floor(Math.random()*202);
    book.thumbnail = "../img/img"+i+".jpg";
    book.Format.format = "MixedMedia";
    book.Format.format = "PaperBack";
    book.Images.thumbnail = "img/thm/Book"+i+".jpg";
    book.Images.backcover = "img/bkCover/Book"+i+".jpg";
    for(var k=0;k<=3;k++){
      book.Images.Preview.Previews[k] = "img/prevew/Book1_"+k+".jpg";
    }
    book.Series = "First Edition";
    book.Edition = 1;
    book.PublisherRating = Math.floor(Math.random()*11);
    book.copyRight = CopyRight[Math.floor(Math.random()*CopyRight.length)];
    book.PhysicalProperties.dimension.height = 12.9;
    book.PhysicalProperties.dimension.width = 19.8;
    book.PhysicalProperties.weight = 5;
    book.price = Math.floor(Math.random()*1011);
    book.save(function(err,books){
      if(err)
        console.log(err);

    });
  }
};
var createCategoryDocument = function(){
  var CategoryArray = ["Syllabus","Tecnical","General_Knowledge","Finance","Mathematics","Competetive"];
  var binary = [0,1];
  for(var i=0;i<CategoryArray.length;i++){
    var categories = new Categories;
    categories.categoryId = i+1;
    categories.Name = CategoryArray[i];
    categories.Description = "This is a goood book ,you will defnitely love it";
    categories.isFrequentlyUsed = binary[Math.floor(Math.random()*binary.length)];
    categories.isMainCategory = 1;
    categories.save(function(err,category){
      if(err)
        console.log(err);

      console.log(category);

    });
  }
};
var createAuthorDocument = function(){
  var authorArray = ["Anand","Bharath","Ajit","Michael","Christopher","Shankara","Madhwa"];
  var binary = [0,1];
  for(var i=0;i<authorArray.length;i++){
    var authors = new Authors;
    authors.authorId = i+1;
    authors.name.fullName.firstName= authorArray[i];
    authors.address.fullAddress.city = "Banglore";
    authors.address.fullAddress.state = "Karnataka";
    authors.address.fullAddress.street = "22 nd main road";
    authors.address.fullAddress.zipcode = 560078;
    authors.address.fullAddress.country ="India";
    authors.Description = "My Name  is a "+authorArray[i]+"  ,you will defnitely love it";
    authors.isMainCategory = binary[Math.floor(Math.random()*binary.length)];
    authors.save(function(err,author){
      if(err)
        console.log(err);

      console.log(author);

    });
  }
};
var createSubCategoryDocument = function(){
  var CategoryArray = ["Syllabus","Tecnical","General_Knowledge","Finance","Mathematics","Competetive"];
  var binary = [0,1];
  for(var i=0;i<CategoryArray.length;i++){
    var categories = new Categories;
    categories.categoryId = i+8;
    categories.Name = CategoryArray[i]+1;
    categories.Description = "This is a goood book ,you will defnitely love it";
    categories.isMainCategory = 0;
    categories.maincategoryName = CategoryArray[i];
    categories.save(function(err,category){
      if(err)
        console.log(err);

      console.log(category);

    });
  }
};
stdio.question('Enter Number Of Book Documents To Be Created', function (err, noOfBooks) {
  createDocument(noOfBooks);
  createCategoryDocument();
  createAuthorDocument();
  createSubCategoryDocument();
  console.log(noOfBooks);
});
