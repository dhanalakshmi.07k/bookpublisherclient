	var mongoose = require('mongoose');
	var stdio = require('stdio');

mongoose.connect('mongodb://localhost/book_publisher');
	var categorySchema = new mongoose.Schema(
	{
     categoryId : Number,
     Name : String,
    Description : String,
    isMainCategory : Number
	},{collection: "book_categories"});

	var authorSchema = new mongoose.Schema(
	{
     authorId : Number,
    Description : String,
     name: {
            fullName: {
                 firstName : String,
            	 lastName : String
            	 }
        },
     address: {
        fullAddress: {
        	 city : String,
            state: String,
            Street: String,
            ZIpCode : Number,
            Country : String
        } 
       }},{collection: "book_authors"});

	var Categories =  mongoose.model('categories',categorySchema);
	var Authors =  mongoose.model('author',authorSchema);

	var createCategoryDocument = function(){
		var CategoryArray = ["Mythology","Thriller","Romantic","Horror","Fictional","Fantasy","Documentary"];
		var binary = [0,1];
		for(var i=1;i<CategoryArray.length;i++){
			var categories = new Categories;
		    categories.categoryId = i+1;
		    categories.Name = CategoryArray[i];
		    categories.Description = "This is a goood book ,you will defnitely love it";
		    categories.isMainCategory = binary[Math.floor(Math.random()*binary.length)];;
		    categories.save(function(err,category){
		    	if(err)
		    		console.log(err);

		    		console.log(category);
		    	
		    });
		}
	};
	var createAuthorDocument = function(){
		var authorArray = ["Anand","Bharath","Ajit","Michael","Christopher","Shankara","Madhwa"];
		var binary = [0,1];
		for(var i=1;i<authorArray.length;i++){
			var authors = new Authors;
		    authors.authorId = i+1;
		    authors.name.fullName.firstName= authorArray[i];
		    authors.address.fullAddress.city = "Banglore";
		    authors.address.fullAddress.state = "Karnataka";
		    authors.address.fullAddress.street = "22 nd main road";
		    authors.address.fullAddress.zipcode = 560078;
		    authors.address.fullAddress.country ="India";
		    authors.Description = "My Name  is a "+authorArray[i]+"  ,you will defnitely love it";
		    authors.isMainCategory = binary[Math.floor(Math.random()*binary.length)];;
		    authors.save(function(err,author){
		    	if(err)
		    		console.log(err);

		    		console.log(author);
		    	
		    });
		}
	};
	createCategoryDocument();
	createAuthorDocument();