var mongoose = require('mongoose');
var stdio = require('stdio');

/*mongoose.connect('mongodb://localhost/book_publisher');*/
mongoose.connect('mongodb://localhost/bookish');
var categorySchema = new mongoose.Schema(
  {
    categoryId : Number,
    categoryName : String,
    categoryDescription : String,
    isMainCategory : Number,
    subCategory : [ {
      name : String,
      description : String,
      icon:String,
      open:Number,
    }]
  },{collection: "book_categories"});

var authorSchema = new mongoose.Schema(
  {
    authorId : Number,
    Description : String,
    isDeletedAuthor:Number,
    name: {
      fullName: {
        firstName : String,
        lastName : String
      }
    },
    address: {
      fullAddress: {
        city : String,
        state: String,
        Street: String,
        ZIpCode : Number,
        Country : String
      }
    }},{collection: "book_authors"});

var Categories =  mongoose.model('categories',categorySchema);
var Authors =  mongoose.model('author',authorSchema);

var createCategoryDocument = function(){

  var CategoryArray = [
                          {
                            categoryName:"SSC",
                            subCategory:['Combined Graduate Level','CAPF','CISF','Delhi Police Examination','Constable (GD)','Stenographer Grade C,D','Jr. Hindi Translator']
                          },
                          {
                            categoryName:"Railway Recruitment Boards",
                            subCategory:['Group-D',' ASM/Goods Gard','Asst. Loco Pilot','ESM']
                          },
                          {
                            categoryName:"IBPS/State Bank",
                            subCategory:['PO','Clerck']
                          }
                      ]
  /*{
 ,
  {categoryName:"Railway Recruitment Boards",subCategory:[{name:"Group-D"},{name:" ASM/Goods Gard"},{name:"Asst. Loco Pilot"},{name:" ESM"}]},
  {categoryName:"IBPS/State Bank",subCategory:[{name:"PO"},{name:" Clerck"}]}

};*/
 /* var CategoryArray = ["SSC ","Railway Recruitment Boards","IBPS/State Bank","Finance","Mathematics","Competetive"];*/
  var icon = ["fa fa-simplybuilt","fa fa-server","fa fa-asterisk","fa fa-hdd-o","fa  fa-spinner"];
  var binary = [0,1];
  for(var i=0;i<CategoryArray.length;i++){
    var categories = new Categories;
    categories.categoryId = i+1;
    categories.categoryName = CategoryArray[i].categoryName;
    categories.icon = "fa fa-simplybuilt";
    categories.open = 1;
    categories.Description = "This is a goood book ,you will defnitely love it";
    categories.isMainCategory = binary[Math.floor(Math.random()*binary.length)];
   /* categories.subCategory = CategoryArray[i].subCategory;*/
   /* console.log(CategoryArray[i].subCategory);*/
    for(var j=0;j<CategoryArray[i].subCategory.length;j++){
      var subCategory = {
      name : CategoryArray[i].subCategory[j],
      description : "sub category of category"+CategoryArray[i]
    };
      categories.subCategory.push(subCategory);
    }
    categories.save(function(err,category){
      if(err)
        console.log(err);

      console.log(category);

    });
  }
};
var createAuthorDocument = function(){
  var authorArray = ["Anand","Bharath","Ajit","Michael","Christopher","Shankara","Madhwa"];
  var binary = [0,1];
  for(var i=1;i<authorArray.length;i++){
    var authors = new Authors;
    authors.authorId = i+1;
    authors.name.fullName.firstName= authorArray[i];
    authors.isDeletedAuthor=1;
    authors.address.fullAddress.city = "Banglore";
    authors.address.fullAddress.state = "Karnataka";
    authors.address.fullAddress.street = "22 nd main road";
    authors.address.fullAddress.zipcode = 560078;
    authors.address.fullAddress.country ="India";
    authors.Description = "My Name  is a "+authorArray[i]+"  ,you will defnitely love it";
    authors.isMainCategory = binary[Math.floor(Math.random()*binary.length)];;
    authors.save(function(err,author){
      if(err)
        console.log(err);

    /*  console.log(author);*/

    });
  }
};
createCategoryDocument();
createAuthorDocument();
