// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var bookSchema = new mongoose.Schema(
  {
    bookId : Number,
    IsbnNumber10 : Number,
    IsbnNumber13 : Number,
    Title : String ,
    ShortDesc : String,
    LongDesc : String,
    PublishedDate : Date ,
    Language : String,
    isPublished:Number,
    PrintingPrice:Number,
    isDeleteBook:Number,
    Category : {
      names : [
        String
      ]
    },
    PageNo : Number,
    Author: {
      names: [
        String
      ]
    },
    Format: {
      format: [
        String
      ]
    },
    Images : [ {
      path : String,
      contentType : String,
      imageName : String
    }],

    Series :String,
    Edition : Number,
    PublisherRating : Number,
    copyRight : String,
    PhysicalProperties: {
      dimension: {
        height:String,
        width:String
      },
      weight: String
    },
    price : Number,
    createdAt:{type:Date,default:Date.now},
    updateAt:{type:Date,default:Date.now}
  },{collection: "Books"});

bookSchema.index({ Title: "text",Author:{names:"text"},Category:{names:"text"}});
mongoose.model('Books', bookSchema);


