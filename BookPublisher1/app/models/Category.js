/**
 * Created by zendynamix on 27-10-2015.
 */

/*var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


categorySchema = new mongoose.Schema(
  {
    categoryName:String,
    categoryDescription:String
  },{collection:'Categories'});

mongoose.model('Categories', categorySchema);*/

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var categorySchema = new mongoose.Schema(
  {
    categoryId: Number,
    categoryName : String,
    categoryDescription : String,
    isMainCategory : Number,
    subCategory : [ {
      name:String,
      description : String,
      categoryName : String
    }],
    createdOn:{type:Date,default:Date.now},
    updateOn:{type:Date,default:Date.now},
  },{collection: "Categories"});
categorySchema.index({ categoryName: 'text'});
mongoose.model('Categories',categorySchema);

