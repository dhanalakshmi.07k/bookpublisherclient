/**
 * Created by zendynamix on 27-10-2015.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var authorSchema = new mongoose.Schema(
  {
    authorId:Number,
    name:{fullName:{firstName:String,lastName:String}},
    address:{fullAddress:{city:String,state:String,Street:String,ZIpCode:Number,Country:String}},
    books:[{IsbnNumber10:Number},{IsbnNumber13:Number}],
    Description:String
  },{collection:'Authors'});
authorSchema.index({name:"text"});
mongoose.model('Authors', authorSchema);




