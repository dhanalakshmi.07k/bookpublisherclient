var express = require('express');
var serverApp = express();
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var cors = require('cors');
var router = express.Router();
var config= require('../../config/config')
var Q = require("q");


module.exports = function (app) {
  app.use(router);

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
};

var Books =  mongoose.model('Books');
var Author =  mongoose.model('Authors');
var Category =  mongoose.model('Categories');
var ContactDetails =  mongoose.model('ContactDetails');
	/*Route Description*/
router.route('/all/limit/:limit')
	.get(function(req,res){
		Books.find(function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Got All books"+req.params.limit);
		}).limit(req.params.limit);
	});
router.route('/book/bookId/:id')
	.get(function(req,res){
		Books.findOne({ bookId: req.params.id},function(err,books){
		if(err)
			res.send(err);
			res.header("Access-Control-Allow-Origin", "*");
  			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			res.send(books);
			console.log("Get book by bookId");
		})
	});
/*router.route('/all/books')
	.get(function(req,res){
		Books.find(function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get books by limit");
		});
	});*/
router.route('/category/:id')
	.get(function(req,res){
		Books.find({ "Category.names": req.params.id},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get books by Category");
		})
	});

router.route('/author/:id')
	.get(function(req,res){
		Books.find({ "Author.names":req.params.id},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get Book By BookId"+req.params.id);
		})
	});

router.route('/all/category')
	.get(function(req,res){
		Category.find({},'isMainCategory categoryName subCategory',function(err,book_category){
		if(err)
			res.send(err);
			res.send(book_category);
			console.log("Get All Category");
		})
	})
	.post(function(req,res){
		var category = new Category();
		category.description = req.body.description;
		category.name = req.body.name;
		category.save(function(err){
		if(err)
			res.send(err);
			res.send(category);
			console.log(category);
		})
	});

router.route('/all/author')
	.get(function(req,res){
		Author.find(function(err,author){
		if(err)
			res.send(err);
			res.send(author);
			console.log("Get All Author");
		});
	})
	.post(function(req,res){
		var author = new Author();
		/*author.authorId = req.body.authorId;
		author.description = req.body.description;*/
		console.log(req.body);
    res.send(req.body);

    /*	author.save(function(err){
          if(err)
          res.send(err);
              res.send(author);
              console.log(req.body);
          })*/
	});
	router.route('/total/book/count')
	.get(function(req,res){
		Books.count({'isPublished':1},function(err,books){
		if(err)
			res.send(err);
			var count = {NoOfBooks: books};
			/*res.header("Access-Control-Allow-Origin", "*");
  			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");*/
  			res.send(count);
			console.log("All books Count"+count);
		});
	});
	router.route('/all/books/range/:start/:end')
	.get(function(req,res){
		Books.find({bookId: {$gte: req.params.start, $lt: req.params.end}},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Got All books");
		}).sort({ sortId: 1 });
	});
	router.route('/main/category')
	.get(function(req,res){
		Category.find({ "isMainCategory":1},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Main Category");
		});
	});
	/*{"$gte": new Date(2012, 7, 14), "$lt": new Date(2012, 7, 15)}*/
	router.route('/all/books/new')
	.get(function(req,res){
		var todaysDate = new Date();
		var oneMonthFromToday = new Date(new Date(todaysDate).setMonth(todaysDate.getYear()-100));
		Books.find({PublishedDate: {$gte: new Date(2015, 1, 1), "$lt": new Date(2016, 7, 15)}},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get All New books books From"+todaysDate+"to"+oneMonthFromToday);
		}).sort({ sortId: 1 });
	});
router.route('/total/book/category/:cName/count')
	.get(function(req,res){
		Books.count({ "Category.names": req.params.cName,"isPublished":1},function(err,books){
		if(err)
			res.send(err);
			var count = {NoOfBooks: books};
  			res.send(count);
			console.log("All books Count By Category"+" "+" "+count.NoOfBooks);
		});
	});

	router.route('/total/book/author/:aName/count')
	.get(function(req,res){
		Books.count({ "Author.names":req.params.aName,"isPublished":1},function(err,books){
		if(err)
			res.send(err);
			var count = {NoOfBooks: books};
  			res.send(count);
			console.log("All books Count By Category"+count);
		});
	});
	router.route('/all/books/category/:cName/range/:start/:end')
	.get(function(req,res){
		Books.find({ "Category.names": req.params.cName,"isPublished":1},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get All books");
		}).sort({ sortId: 1 }).skip(parseInt(req.params.start)).limit(parseInt(req.params.end));
	});
	router.route('/all/books/author/:aName/range/:start/:end')
	.get(function(req,res){
		Books.find({ "Author.names":req.params.aName},function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get All books ");
		}).sort({ sortId: 1 }).skip(parseInt(req.params.start)).limit(parseInt(req.params.end));
	});
	router.route('/all/books/range/:start/:end')
	.get(function(req,res){
		Books.find({},'bookId Title Author PublishedDate ShortDesc Images',function(err,books){
		if(err)
			res.send(err);
			res.header("Access-Control-Allow-Origin", "*");
  			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			res.send(books);
			console.log("Get books by limit");
		}).sort({ sortId: 1 }).skip(parseInt(req.params.start)).limit(parseInt(req.params.end));
	});


router.route('/all/books/:start/:end')
  .get(function(req,res){
    Books.find({"isPublished":1},{ "sortId":1,"bookId": 1, "Images": { "$slice": 1 }, "Title":3,"Author":4,"PublishedDate":5,"ShortDesc":6,"isDeleteBook":7,"isPublished":8},
      function(err,books){
      if(err)
        res.send(err);
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      console.log("++++++++++++++++++++++++");
      console.log(books);
      if(books.Images){
        console.log(books.Images.length);
      }
      res.send(books);

      console.log("Get books by limit");

    }).sort({ sortId: 1 }).skip(parseInt(req.params.start)).limit(parseInt(req.params.end)).sort({ createdAt: -1 });
  });

router.route('/book/:id/image/previews')
	.get(function(req,res){
		Books.find({"bookId":req.params.id},'Images.Preview.Previews',function(err,books){
		if(err)
			res.send(err);
			res.send(books);
			console.log("Get Books Preview Images");
		}).sort({ sortId: 1 }).skip(parseInt(req.params.start)).limit(parseInt(req.params.end));
	});

router.route('/category/mainCategory/:cname')
  .get(function(req,res){
    Category.find({ "maincategoryName":req.params.cname},
      function(err,books){
        if(err)
          res.send(err);
        res.send(books);
        console.log("Get Sub-Categories For"+req.params.cname);
    });
  });

router.route('/category/ismainCategory')
  .get(function(req,res){
    Category.find({ "isMainCategory":1},
      function(err,books){
        if(err)
          res.send(err);
        res.send(books);
        console.log("Get Sub-Categories For"+req.params.cname);
    });
  });

router.route('/books/category/categories/:categoryArray')
  .get(function(req,res){
  	console.log(req.params.categoryArray);
  	var arrray = [req.params.categoryArray];
    Books.find({'Category.names': { $in: [req.params.categoryArray]}}/*['Syllabus1', 'Technical','Technica2']*/
      ,'bookId Title Images',function(err,books){
        if(err)
          res.send(err);
        res.send(books);
        console.log(books);
    }).sort({ sortId: 1 });
  });

router.route('/books/category/categoriesByBook/:bookId')
  .get(function(req,res){
  	var categoryArray = [];
  	Books.findOne({ bookId: req.params.bookId},'Category.names',function(err,books){
		if(err){
			res.send(err);
		}else{
			console.log(books);
			console.log(books.Category);
			res.send(books.Category.names);
			/*categoryArray = books.Category.names;*/
			/*Books.find({'Category.names': { $in: categoryArray}}
		      ,function(err,booksByCategory){
		        if(err)
		          res.send(err);
		        res.send(booksByCategory);
		        console.log(booksByCategory);
		    });*/
			}
		})
  });


router.route('/search')
  .get(function(req,res){
    console.log(req.query.search);
    Books.find(   { $and: [ { Title: {$regex:req.query.search, $options:'i'} },{"isPublished":1} ] }
      ,{"_id":0,"Title":1},function(err,books){
      console.log("inside search");
      console.log(books);
      res.send(books);
    }).sort({ sortId: 1 }).limit(5);
  })


router.route('/search/book/name/:searchName/:skip/:limit')
  .get(function(req,res){
    var searchParameter="\""+req.params.searchName+"\"";
    Books.find( { $and:[{$text: {$search: searchParameter}},{"isPublished":1} ] }
      ,function(err,books){
      res.send(books);
    }).sort({ sortId: 1 }).skip(parseInt(req.params.skip)).limit(parseInt(req.params.limit));
  })

router.route('/search/book/count/name/:searchName')
  .get(function(req,res){
    var searchParameter="\""+req.params.searchName+"\"";
    Books.count({$text:{$search:searchParameter}},function(err,books){
      var count = {NoOfBooks: books};
      res.send(count)
    });
  })

router.route('/book/title/')
  .get(function(req,res){
    Books.find({},'Title',function(err,bookTitle){
      if(err)
        res.send(err);
      res.send(bookTitle);
    })
  })


router.route('/main/category/categoryName/')
  .get(function(req,res){
    Category.find({},{_id:0,categoryName:1},function(err,categroyName){
        if(err)
          res.send(err);
        res.send(categroyName);

      })
  });

router.route('/imageBaseURL')
  .get(function(req,res){
    res.send(config.imageBaseUrl);
  })

router.route('/index.html')
  .get(function(req,res){
    res.send("");
  })

router.route('/book/regex/title/:searchBookTitle')
  .get(function(req,res){
    console.log(req.params.searchBookTitle);
    Books.aggregate(

      // Pipeline
      [
        // Stage 1
        {
          $match: {
            "Title":{$regex:req.params.searchBookTitle,$options:'i'}
          }
        },

        // Stage 2
        {
          $limit: 15
        },

        // Stage 3
        {
          $group: {
            "_id":"$Title"
          }
        },

        // Stage 4
        {
          $project: {
            "_id":0,
            "value":"$_id"
          }
        }

      ],function(err,bookTitle){
        if(err)
          res.send(err);
        res.send(bookTitle);
      });
      // Created with 3T MongoChef, the GUI for MongoDB - http://3t.io/mongochef
  })

router.route('/book/regex/Author/:searchBookAuth')
  .get(function(req,res){
    console.log(req.params.searchBookAuth);
   /* Author.find({'nickName':{$regex:req.params.searchBookAuth,$options:'i'}},function(err,bookAuth){
      if(err)
        res.send(err);
      res.send(bookAuth);
    })*/
    Author.aggregate(

      // Pipeline
      [
        // Stage 1
        {
          $match: {
            "nickName":{$regex:req.params.searchBookAuth,$options:'i'}
          }
        },

        // Stage 2
        {
          $limit: 10
        },

        // Stage 3
        {
          $group: {
            "_id":"$nickName"
          }
        },

        // Stage 4
        {
          $project: {
            "_id":0,
            "value":"$_id"
          }
        }

      ],function(err,bookAuth){
      if(err)
        res.send(err);
      res.send(bookAuth);
    });
  })

router.route('/book/regex/Category/:searchBookCategory')
  .get(function(req,res){
    console.log(req.params.searchBookCategory);
   /* Category.aggregate(

      // Pipeline
      [
        // Stage 1
        {
          $match: {
            "categoryName":{$regex:req.params.searchBookCategory, $options: '"$i"'}
          }
        },

        // Stage 2
        {
          $group: {
            "_id":"$categoryName"
          }
        },

        // Stage 3
        {
          $match: {

          }
        },

        // Stage 4
        {
          $project: {
            "_id":0,
            "value":"$_id"
          }
        }

      ],function(err,bookCat){
        if(err)
          res.send(err);
        res.send(bookCat);
      });*/

      // Created with 3T MongoChef, the GUI for MongoDB - http://3t.io/mongochef

     Category.aggregate(

       // Pipeline
       [
         // Stage 1
         {
           $unwind: "$subCategory"
         },

         // Stage 2
         {
           $match: {
               "subCategory.name":{$regex:req.params.searchBookCategory, $options: '"$i"'}
           }
         },

         // Stage 3
         {
           $limit: 15
         },

         // Stage 4
         {
           $group: {
             "_id":"$subCategory.name"
           }
         },

         // Stage 5
         {
           $project: {
             "_id":0,
             "value":"$_id"
           }
         }

       ],function(err,bookCat){
         if(err)
           res.send(err);
         res.send(bookCat);
       });
      // Created with 3T MongoChef, the GUI for MongoDB - http://3t.io/mongochef
  })

//creating all books search
router.route('/books/search/all/:searchBookTitle/:mainCategory/:searchBookAuthor')
  .get(function(req,res){
  console.log(req.params.searchBookTitle);
  console.log(req.params.searchBookAuthor);
    console.log(req.params.mainCategory);
    var title = req.params.searchBookTitle,
    author = req.params.searchBookAuthor,
    category =req.params.mainCategory
    var promises = [];
    var typeAheadObject;
    var typeAheadArray = [];
    var defered = Q.defer();

    var  promise1 = getBooksByTitleForSearch(title);
    var promise2 = getBooksByCategoryForSearch(category);
    var promise3 = getBookByAuthorForSearch(author);

      function getBooksByTitleForSearch(title){
      Books.aggregate(

        // Pipeline
        [
          // Stage 1
          {
            $match: {
              "Title":{$regex:title,$options:'i'}
            }
          },

          // Stage 2
          {
            $limit: 15
          },

          // Stage 3
          {
            $group: {
              "_id":"$Title"
            }
          },

          // Stage 4
          {
            $project: {
              "_id":0,
              "value":"$_id",
              "type": {$literal: 'title'}
            }
          }

        ],function(err,bookTitle){
          if(err){
            defered.reject(err)
          }
          else{
            for(var i = 0;i<bookTitle.length;i++){
              typeAheadObject = bookTitle[i];
              typeAheadArray.push(typeAheadObject);
            }
            defered.resolve(typeAheadArray);
          }
        })
    }
    function getBookByAuthorForSearch(author){
      Author.aggregate(

        // Pipeline
        [
          // Stage 1
          {
            $match: {
              "nickName"	:{$regex:author,$options:'i'}
            }
          },

          // Stage 2
          {
            $limit: 8
          },

          // Stage 3
          {
            $group: {
              "_id":"$nickName"
            }
          },

          // Stage 4
          {
            $project: {
              "_id":0,
              "value":"$_id",
              "type": {$literal: 'author'}
            }
          }

        ],function(err,authDetails){
          if(err){
            defered.reject(err)
        }
          else{
            for(var i = 0;i<authDetails.length;i++){
              typeAheadObject = authDetails[i];
              typeAheadArray.push(typeAheadObject);
            }
            defered.resolve(typeAheadArray);
          }
        })
    }
    function getBooksByCategoryForSearch(category){
      Category.aggregate(

        // Pipeline
        [
          // Stage 1
          {
            $unwind: "$subCategory"
          },

          // Stage 2
          {
            $match: {
              "subCategory.name":{$regex:req.params.searchBookCategory, $options: '"$i"'}
            }
          },

          // Stage 3
          {
            $limit: 10
          },

          // Stage 4
          {
            $group: {
              "_id":"$subCategory.name"
            }
          },

          // Stage 5
          {
            $project: {
              "_id":0,
              "value":"$_id",
              "type":{$literal: 'category'}
            }
          }

        ],function(err,bookCat){
          if(err){
            defered.reject(err);
          }
          else{
            for(var i = 0;i<bookCat.length;i++){
              typeAheadObject = bookCat[i];
              typeAheadArray.push(typeAheadObject);
            }
            defered.resolve(typeAheadArray);
          }

        })
  }
    Q.all([promise1, promise2, promise3]).then(function(err,value){
      if(err){
        res.send(err);
      }
      else{
        res.send(value)
      }
    })
    getBooksByFilter =function(title,author,category){
      Books.aggregate(

        // Pipeline
        [
          // Stage 1
          {
            $match: {
              "Title":{$regex:title,$options:'i'}
            }
          },

          // Stage 2
          {
            $limit: 15
          },

          // Stage 3
          {
            $group: {
              "_id":"$Title"
            }
          },

          // Stage 4
          {
            $project: {
              "_id":0,
              "value":"$_id",
              "type": {$literal: 'title'}
            }
          }

        ],function(err,bookTitle){
          if(err)
            defered.reject(err);
          else{
            for(var i = 0;i<bookTitle.length;i++){
              typeAheadObject = bookTitle[i];
              typeAheadArray.push(typeAheadObject);
            }
            defered.resolve(typeAheadArray);
            promises.push(defered.promise);
            if(promises.length ===bookTitle.length ){
              Author.aggregate(

                // Pipeline
                [
                  // Stage 1
                  {
                    $match: {
                      "nickName"	:{$regex:author,$options:'i'}
                    }
                  },

                  // Stage 2
                  {
                    $limit: 8
                  },

                  // Stage 3
                  {
                    $group: {
                      "_id":"$nickName"
                    }
                  },

                  // Stage 4
                  {
                    $project: {
                      "_id":0,
                      "valu":"$_id",
                      "type": {$literal: 'author'}
                    }
                  }

                ],function(err,authDetails){
                  if(err){
                    defered.reject(err);
                  }
                  else{
                    for(var i = 0;i<authDetails.length;i++){
                      typeAheadObject = authDetails[i];
                      typeAheadArray.push(typeAheadObject);
                    }
                    defered.resolve(typeAheadArray);
                    promises.push(defered.promise);
                    if(promises.length === authDetails.length){
                      Category.aggregate(

                        // Pipeline
                        [
                          // Stage 1
                          {
                            $match: {
                              "categoryName":category
                            }
                          },

                          // Stage 2
                          {
                            $project: {
                              "_id":"$subCategory.name"
                            }
                          },

                          // Stage 3
                          {
                            $project: {
                              "_id":0,
                              "subCatArray":"$_id"
                            }
                          }

                        ],function(err,subCatDetails){
                          if(err){
                            defered.reject(err);
                          }
                          else{
                            console.log(subCatDetails[0]);
                            Books.find({"Category.names":{ $in: subCatDetails[0].subCatArray}},function(err,bookDet){
                              if(err){
                                res.send(err)
                              }
                              for(var i = 0;i<bookDet.length;i++){
                                typeAheadObject = bookDet[i];
                                typeAheadArray.push(typeAheadObject);
                              }
                              defered.resolve(typeAheadArray);
                              promises.push(defered.promise);
                            })
                          }
                        })
                    }
                  }
                });
            }
          }
        });
      return Q.all(promises);
    }
})

router.route('/book/by/title/:bookTitle')
  .get(function(req,res){
   Books.findOne({ "Title":req.params.bookTitle},function(err,bookDtls){
     if(err){
       res.send(err)
     }
     res.send(bookDtls);
   })
  })

router.route('/books/category/:mainCategory')
  .get(function(req,res){
    /*Category.findOne({"categoryName":req.params.mainCategory},"subCategory.name",function(err,subCategoryDetails){
      if(err){
        res.send(err)
      }
      console.log(subCategoryDetails);
      /!*res.send(subCategoryDetails);*!/
      console.log(subCategoryDetails.subCategory)
      var subCatArray = [];
      for(var i = 0;i<subCategoryDetails.subCategory.length;i++){
          var subCatObject = subCategoryDetails.subCategory[i].name;
        subCatArray.push(subCatObject);
      }
      console.log(subCatArray);
     /!* res.send(subCatArray);*!/
    /!*subCategoryName = [];*!/
     var subCategoryName = subCatArray;
      console.log(subCategoryName)
      Books.find({"Category.names":{ $in: subCategoryName}},function(err,bookDet){
        if(err){
          res.send(err)
        }
        console.log(bookDet);
        res.send(bookDet)
      })
    })*/
    Category.aggregate(

      // Pipeline
      [
        // Stage 1
        {
          $match: {
            "categoryName":req.params.mainCategory
          }
        },

        // Stage 2
        {
          $project: {
            "_id":"$subCategory.name"
          }
        },

        // Stage 3
        {
          $project: {
            "_id":0,
            "subCatArray":"$_id"
          }
        }

      ],function(err,subCatDetails){
        if(err)
          res.send(err);
        console.log(subCatDetails[0]);
        Books.find({"Category.names":{ $in: subCatDetails[0].subCatArray}},function(err,bookDet){
          if(err){
            res.send(err)
          }
          console.log(bookDet);
          res.send(bookDet)
        })
      });
      // Created with 3T MongoChef, the GUI for MongoDB - http://3t.io/mongochef
  })


