'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);
  var reloadPort = 35729, files;
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    production: {
      server: {
        file: 'AdminDistribution/app.js'
      }
    },
    develop: {
      server: {
        file: 'app.js'
      }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      js: {
        files: [
          'app.js',
          'app/**/*.js',
          'config/*.js'
        ],
        tasks: ['develop', 'delayed-livereload']
      },
      css: {
        files: [
          'public/css/*.css'
        ],
        options: {
          livereload: reloadPort
        }
      },
      views: {
        files: [
          'app/views/*.jade',
          'app/views/**/*.jade'
        ],
        options: { livereload: reloadPort }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          '../AdminDistribution/public/app/libaries/minifiedScripts/jQuery-File-Upload-master-min-all.js': ['public/app/libs/jQuery-File-Upload-master/js/jquery.ui.widget.js','public/app/libs/jQuery-File-Upload-master/js/jquery.fileupload.js','public/app/libs/jQuery-File-Upload-master/js/jquery.fileupload-process.js','public/app/libs/jQuery-File-Upload-master/js/jquery.fileupload-image.js','public/app/libs/jQuery-File-Upload-master/js/jquery.fileupload-validate.js','public/app/libs/jQuery-File-Upload-master/js/jquery.fileupload-angular.js'],
          '../AdminDistribution/public/app/libaries/minifiedScripts/angular-animate-min.js':['public/app/libs/pagination/angular-animate.js'],
          '../AdminDistribution/public/app/libaries/minifiedScripts/angular-chosen-min.js': ['public/app/libs/Multiselect/angular-chosen.js'],
          '../AdminDistribution/public/app/libaries/minifiedScripts/appScripts-min.js': ['public/app/Scripts/controllers/LoginController.js','public/app/Scripts/serviceFactory/AuthenticationService.js','public/app/Scripts/controllers/AuthorController.js','public/app/Scripts/serviceFactory/AuthorService.js','public/app/Scripts/controllers/CategoryLabelController.js','public/app/Scripts/controllers/CategoryController.js','public/app/Scripts/controllers/BookController.js','public/app/Scripts/controllers/authourDropDownController.js','public/app/Scripts/controllers/categoryDropDownController.js','public/app/Scripts/serviceFactory/BookService.js','public/app/Scripts/serviceFactory/CategoryService.js','public/app/Scripts/router/router.js','public/app/Scripts/Directives/autoCompleteDirective.js','public/app/Scripts/Directives/addCategoryLabelDirective.js','public/app/Scripts/Directives/addCategoryDirective.js','public/app/Scripts/Directives/addAuthorDirective.js'],
          '../AdminDistribution/public/app/libaries/minifiedScripts/angular-wysiwyg.js': ['public/app/libs/texteditor/scripts/angular-wysiwyg.js','public/app/libs/texteditor/scripts/bootstrap-colorpicker-module.js']
        }
      }
    },

    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          '../AdminDistribution/public/app/libaries/cssMinified/jQuery-File-Upload-master-min-all.css': ['public/app/libs/jQuery-File-Upload-master/css/*.css'],
          '../AdminDistribution/public/app/libaries/cssMinified/all-skins.min.css': ['public/app/Style/*.css'],
          '../AdminDistribution/public/app/libaries/cssMinified/texteditor.min.css': ['public/app/libs/texteditor/css/style.css']
        }
      }
    },
    processhtml: {
      build: {
        files: {
          '../AdminDistribution/public/index.html': [' public/index.html']
        }
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, cwd: 'app/', src: ['**'], dest: '../AdminDistribution/app/'},
          {expand: true, cwd: 'config/', src: ['**'], dest: '../AdminDistribution/config/'},
          {expand: true, cwd: 'public/', src: ['**','!**/Scripts/**','!**/Style/**','!**/Style-min/**','!index.html'], dest: '../AdminDistribution/public/'},
          { src: "package.json", dest: '../AdminDistribution/'},
          { src:"app.js", dest: '../AdminDistribution/'},
          { src: "server.crt", dest: '../AdminDistribution/'},
          { src:"server.key", dest: '../AdminDistribution/'},

        ]
      }
    }
  });

  grunt.config.requires('watch.js.files');
  files = grunt.config('watch.js.files');
  files = grunt.file.expand(files);
  grunt.loadNpmTasks('grunt-contrib-uglify'); // load the given tasks
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function(err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded)
            grunt.log.ok('Delayed live reload successful.');
          else
            grunt.log.error('Unable to make a delayed live reload.');
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('default', ['develop', 'watch']);
  grunt.registerTask('production', ['uglify','cssmin','copy','processhtml']);



};
