var express = require('express');
var glob = require('glob');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var fs = require('fs');
/*var http = require('http');
var https = require('https');*/
var multer  = require('multer');
//passport integration
var engines = require('consolidate');
var path = require('path')
module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.use(compress());
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';
 /* app.set('superSecret', config.secret); */// secret variable
  app.engine('html', engines.mustache);
  app.set('view engine', 'html');
  app.set('views', config.root + '/app/views');
 /* app.set('views', config.root + '/app/views');*/
  app.set('view engine', 'jade');
  app.use(bodyParser.json());


  app.use('/app/view/*', function(req, res, next) {
    if (!req.headers['authorization'] ) {
console.log(req.headers['Bearer']);
      res.sendfile('app/views/Error.html');

    } else {
      next();
    }
  });

  app.use(express.static(path.join(__dirname, 'public')));
  app.use(express.static(__dirname+'/../public'));
  app.use(express.static(__dirname+'../../../../bookish',{maxAge:"356d"}));
  var realpath = fs.realpathSync(__dirname+'../../../uploads/');

  app.use(multer({ dest:realpath}));
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(express.static(config.root + '/public/app'));
  app.use(methodOverride());

  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });


  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });

};
