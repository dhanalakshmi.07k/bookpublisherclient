var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'AdminBookPublisher'
    },
    port: 8000,
    db:'mongodb://localhost/bookish',
    imageBaseUrl:' https://s3.amazonaws.com/dev-image-folder/images/',
    imageFolderName:"images/",
    imageBucketName:"dev-image-folder"

  },

  test: {
    root: rootPath,
    app: {
      name: 'AdminBookPublisher'
    },
    port: 8000,
    db: 'mongodb://localhost/bookish-test',
    imageBaseUrl:'https://s3-ap-southeast-1.amazonaws.com/lucent-book-image/images/',
    imageFolderName:"images/",
    imageBucketName:"lucent-book-image"
  },

  production: {
    root: rootPath,
    app: {
      name: 'AdminBookPublisher'
    },
    port: 8000,
    db: 'mongodb://development:devPassw0rd@ds051990.mongolab.com:51990/bookish',
    imageBaseUrl:'https://s3-ap-southeast-1.amazonaws.com/lucent-book-image/images/',
    imageFolderName:"images/",
    imageBucketName:"lucent-book-image"
  },

 /* 'secret': 'zendynamix',*/

};

module.exports = config[env];
