app.config(function($routeProvider) {
  $routeProvider

    .when('/', {
      templateUrl: './app/UnSecureView/Login.html'
    })

    .when('/signUp',{
      templateUrl:'./app/UnSecureView/signUp.html'
    })

    .when('/login',{
      templateUrl:'./app/UnSecureView/Login.html'
    })

    .when('/logout',{
      controller:'LoginController',
      templateUrl: './app/UnSecureView/Login.html'
    })








      /*authors*/
      .when('/authorDetails',{
        controller:"AuthorController",
        templateUrl:'./app/view/AuthorView.html'
      })
















  /*categoryLabel*/
      .when('/categoryLabelDetails',{
        controller:'CategoryControllerLabel',
        templateUrl:'./app/view/CategoryLabel.html',

      })









      /*category*/
      .when('/category', {
        controller:'CategoryController',
        templateUrl: './app/view/Category.html'

      })




      /*books*/
      .when('/getAllBooksToCart',{
          controller:'bookController',
          templateUrl:'./app/view/displayBookList.html'

      })

      .when('/getAllBooksToList',{
          controller:'bookController',
          templateUrl: './app/view/bookList.html'

      })

      .when('/addBooks', {
          controller: 'bookController',
          templateUrl: './app/view/AddBook.html'
      })

      .when('/editBookDetails/:editBookId',{
          controller:'bookController',
          templateUrl:'./app/view/AddBook.html'
      })

     /* .when('/editBook',{
          templateUrl:'./app/view/AddBook.html'
      })*/

      .when('/sortBooks', {
          controller:'bookController',
          templateUrl: './app/view/sortBookList.html'

      })




      .otherwise({redirectTo:'/'});
})
