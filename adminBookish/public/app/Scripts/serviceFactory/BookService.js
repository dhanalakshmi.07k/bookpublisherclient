/**
 * Created by zendynamix on 11-04-2016.
 */
app.factory("bookService", function ($http) {
    var selectedPage = 1;
    var scopeVariable=1;
    var bookService = {
        getSelectedPageNo:function(){
            return selectedPage
        },
        setSelectedPageNo:function(pageNo){
            selectedPage=pageNo;
        },
        getBooksForPagination:function(startLimit,endLimit){
            return $http.get("/api/books/"+startLimit+"/"+endLimit);
        },
        getNumberOfBooks:function(){
            return $http.get("/api/books/count");
        },

        setScopeVariable:function(localScopeVariable){
            scopeVariable  =localScopeVariable;
        },
        getScopeVariable:function(){
          return scopeVariable;
        },
        getBookByIdDb: function(id) {
            return $http.get("/api/book/"+id );
        },
        getUnPublishedSelectedPageNo:function(){
            return unPublishSelectedPage
        },

        publishBookById:function(bookPublishId){
            return $http.post("/api/publish/book/"+bookPublishId);
        },
        unPublishBookById:function(bookunPublishId){
            return $http.post("/api/unPublish/book/"+bookunPublishId);
        },
        unarchiveById : function(id) {
            return $http.post("/api/unarchive/Book/"+id );
        },
        deleteBookByIdDb : function(id) {
            return $http.post("/api/delete/book/"+id );
        },
        getBookTitleForSorting:function(){
            return $http.get("/sort/all/books/");
        },
        storeSortedBookId:function(masterobj){
            return $http.post("/book/update/bySort/",masterobj);
        },
        setPublishedSelectedPageNo:function(pageNo){
            publishSelectedPage=pageNo;
        },
        getPublishedBook:function(start,end){
            return $http.get("/all/published/books/"+start+"/"+end);
        },
        getTotalPublishedBookCount:function(){
            return $http.get("/publish/books/count");
        },
        getPublishedSelectedPageNo:function(){
            return publishSelectedPage
        },
        setUnPublishedSelectedPageNo:function(pageNo){
            unPublishSelectedPage=pageNo;
        },
        getUnPublishedSelectedPageNo:function(){
            return unPublishSelectedPage
        },
        getTotalUnPublishedBookCount:function(){
            return $http.get("/unPublish/books/count");
        },
        getUnPublishedBook:function(start,end){
            return $http.get("/all/Unpublished/books/"+start+"/"+end);
        },

        getImagesByBookID:function(bookId){
            return $http.get("/all/images/ById/"+bookId);
        },
        getS3ImagePath:function(){
            return $http.get("/imageBaseURL")
        },
        deleteBookImageById:function(imageName,bookId){
            return $http.get("/delete/image/id/"+imageName+"/"+bookId);
        },

        }
    return bookService;
})
