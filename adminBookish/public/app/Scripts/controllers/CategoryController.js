/**
 * Created by zendynamix on 10-04-2016.
 */
app.controller('CategoryController', function ($scope, $modal, dataService,authInterceptor,$http) {
    $scope.bigCurrentPageCategory = 1;
    $scope.searchPaginationCategory=0;
    $scope.$on('categoryAdded', function (event, args) {
        var curPage=args.currentPage;
        $scope.categoryPagination(curPage);
    });
    $scope.init=function(){
        $scope.categoryPagination($scope.bigCurrentPageCategory);
    }
    $scope.clearSearchResult = function(pageNumber){
        $scope.searchParameter=null;
        dataService.setPaginationPage(pageNumber);
        var pageSelected = 1;
        var startPage,endPage=10;
        if (pageNumber==1) {
            startPage=0;
        }
        else{
            startPage=(pageNumber-1)*10;
        }
        dataService.getSubCategoriesForPagination(startPage,endPage).then(
            function (payload) {
                $scope.categoryDetails = payload.data;
                $scope.getSubcategoryPaginationCount();
            })
    }

    $scope.getSubcategoryPaginationCount=function(){
        dataService.getSubCategoriesPaginationCount().then(
            function (payload) {
                $scope.subCategoryPaginationCount = payload.data[0].count;
            })
    }
    $scope.categoryPagination = function(pageNumber){
        dataService.setPaginationPage(pageNumber);
        var pageSelected = 1;
        var startPage,endPage=10;
        if (pageNumber==1) {
            startPage=0;
        }
        else{
            startPage=(pageNumber-1)*10;
        }
        dataService.getSubCategoriesForPagination(startPage,endPage).then(
            function (payload) {
                $scope.categoryDetails = payload.data;
                $scope.getSubcategoryPaginationCount();
            })
    }
    $scope.getAllMainCategories= function(){
        dataService.getAllMainName().then(function(response){
            $scope.mainCategoryList= response.data;
        });
    }
    $scope.openDeleteCategory = function (subCategoryDetails) {
        $scope.subCategoryDetails=subCategoryDetails;
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/DeleteCategory.html',
            controller: 'categoryDialogController',
            size: 'lg',
            animation: true,
        });
    }
    $scope.categorSearchUrl="/search/categoryLabel";
    function encodingFunction(CategoryLabelName) {
        var res = encodeURIComponent(CategoryLabelName);
        return res;
    }
    $scope.searchCategories = function(searchName) {
        $scope.searchCategoryName=searchName;
        dataService.setsearchCategoryName(searchName);
        var categorySearchParameter=dataService.getsearchCategoryName();
        var encodedNewSubCategory= encodingFunction(searchName);
        $scope.searchPaginationCategory=1;
        $http.get("/search/categoryLabel/cetgoryName/"+encodedNewSubCategory).then(function(response) {
            var firstTenSubCategories=response.data.slice(0, 10);
            $scope.categoryDetails = firstTenSubCategories;
            $scope.subSearchCategoryPaginationCount=response.data.length;
        });
    };
    $scope.categorySearchPagination = function(pageNo,searchCategoryName){
        dataService.setSearchSelectedPageNo(pageNo);
        var currentSearchPage=dataService.getSearchSelectedPageNo();
        var pageSelected = 1;
        var startPage,endPage=10;
        if (pageNo==1) {
            startPage=0;
        }
        else{
            startPage=(pageNo-1)*10;
        }
        dataService.getSearchSubCategoriesForPagination(searchCategoryName,startPage,endPage).then(
            function (payload) {
                $scope.categoryDetails = payload.data;
            })
    }
    $scope.init();


})

app.controller('categoryDialogController', function ($scope, $modalInstance,dataService,$http) {
    $scope.close = function () {
        $modalInstance.close();
    };
    function encodingFunction(CategoryLabelName) {
        var res = encodeURIComponent(CategoryLabelName);
        return res;
    }
    $scope.deleteSubCategoryDetails=function(category){
        var encodedcategoryName= encodingFunction(category.categoryName);
        var encodedsubCategoryName= encodingFunction(category.subCategory.name);
        dataService.deleteSubcategory(encodedcategoryName,encodedsubCategoryName).then(
            function (payload) {
                dataService.deleteSubcategorySanpShot(encodedsubCategoryName).then(
                    function (result) {

                        var currentPage=dataService.getPaginationPage();
                        $scope.categoryPagination(currentPage);
                         if( $scope.searchPaginationCategory===1){
                         var currentSearchPage =dataService.getSearchSelectedPageNo();
                         var categorySearchParameter=dataService.getsearchCategoryName();
                         $scope.categorySearchPagination(currentSearchPage,categorySearchParameter);
                         }
                        $modalInstance.close();
                    })
            })
    }

})
