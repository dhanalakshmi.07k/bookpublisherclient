/**
 * Created by zendynamix on 10-04-2016.
 */
app.controller('CategoryControllerLabel', function ($scope,$location,$modalStack, $modal,$routeParams,$http, dataService,authInterceptor) {
    $scope.itemsperpage = 10;
    $scope.bigTotalLabel=1;
    $scope.bigCurrentPageLabel = 1;
    $scope.init=function(){
        $scope.setPageForLabel($scope.bigCurrentPageLabel);
    }
    $scope.$on('categoryLabelAdded', function (event, args) {
        var curPage=args.currentPage;
        $scope.setPageForLabel(curPage);
    });
    function encodingFunction(uri) {
        var res = encodeURIComponent(uri);
        return res;
    }
    $scope.getTotalCountLabel = function(){
        dataService.getMainCategoryCount().then(function(response){
            $scope.bigTotalLabel= response.data[0].count;
            dataService.setTotalCategoryLabel(response.data[0].count);
        });
    }
    $scope.setPageForLabel = function(pageNo){
        $scope.getTotalCountLabel();
        dataService.setCurrentCategoryLabel(pageNo)
        var pageSelected = 1;
        if (pageNo) {
            pageSelected = pageNo;
        }
        var startPage = (pageSelected - 1) * $scope.itemsperpage;
        dataService.getMainCategoriesForPagination(startPage,$scope.itemsperpage).then(
            function (payload) {
                $scope.categoryDetails = payload.data;
            })
    }
    $scope.openDeleteCategory = function (labelId) {
        //to check whether the category label contains any sub category
        dataService.CategoryByIdDb(labelId.categoryId).then(
            function (resultdata) {
                $scope.labelDeleteData = resultdata.data[0];
                if( $scope.labelDeleteData.subCategory.length!=0) {
                    $scope.subcategoryAvaliable=1;
                }
                else{
                    $scope.labelIdDeleteData = resultdata.data[0];
                    $scope.subcategoryAvaliable=0;
                }

            })

        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/DeleteCategoryLabel.html',
            controller: 'categoryLabelDialogController',
            size: 'lg',
            animation: true
        });
    }
    $scope.getCategoryBasedOnLabel=function(categoryLabelName){
        var encodeInputData= encodingFunction(categoryLabelName.categoryName);
        var subCategoryName=[];
        dataService.getSubCategories(encodeInputData).then(
            function (payload) {
                for(var p=0;p<payload.data[0].subCategory.length;p++){
                    var obj={
                        subcategoryName:payload.data[0].subCategory[p].name,
                        objectId:payload.data[0].subCategory[p]._id,
                        mainCategoryName:categoryLabelName.categoryName,
                        sortId:payload.data[0].subCategory[p].sortId

                    }
                    subCategoryName.push(obj);
                }
                $scope.subCategroyObjectArray=subCategoryName;
                $scope.sortSubcategories( $scope.subCategroyObjectArray);
            })


    }
    $scope.sortSubcategories = function (subCategoryArray) {
        $scope.subCategoryname=subCategoryArray;
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/sortsubCatgeory.html',
            controller: 'CategoryControllerLabel',
            size: 'lg',
            animation: true
        });
    }
    $scope.closeCategoryModel = function () {
        $modalStack.dismissAll();
    };
    $scope.saveSubcategoryOrder= function (sortedSubcategoryArray) {
        var subcategory=[];
        for(var h=0;h<sortedSubcategoryArray.length;h++){
            var subcategoryObj={
                subcategoryName:sortedSubcategoryArray[h].subcategoryName,
                objectId:sortedSubcategoryArray[h].objectId,
                mainCategoryName:sortedSubcategoryArray[h].mainCategoryName,
                subCategorySortId:h
            }
            subcategory.push(subcategoryObj);
        }
        saveSubCategoryToDb(subcategory);
    }
    function saveSubCategoryToDb(subcategory){
        var masterobj={
            sortedSubcategoryArray:subcategory
        };
        dataService.storeSubCategorySortId(masterobj).then(function(response){
            $modalStack.dismissAll();
        });
    }

    $scope.init();
})

app.controller('categoryLabelDialogController', function ($scope, $modalInstance,dataService,$http) {
    $scope.close = function () {
        $modalInstance.close();
    };
    function encodingFunction(uri) {
        var res = encodeURIComponent(uri);
        return res;
    }
    $scope.deleteLabel = function (labelDeleteData) {
        dataService.deleteCategoryByIdDb(labelDeleteData.categoryId).then(
            function (resultdata) {
                var currentPage=dataService.getCurrentCategoryLabel();
                $scope.setPageForLabel(currentPage);
                $modalInstance.close();
            })
    }





});

