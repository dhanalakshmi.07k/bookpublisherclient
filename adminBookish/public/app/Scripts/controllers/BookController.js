/**
 * Created by zendynamix on 11-04-2016.
 */

app.controller('bookController', function ($scope,dataService,authorService,$http,$modal,$modalStack,$routeParams,bookService,$window,authInterceptor,$location) {
    /*incial configuration data*/
    $scope.selectedDropDownvalue="All";
    $scope.alerts = [
        { type: 'success', msg: 'Updated Successfully.' }
    ];
    $scope.menu = [
        ['bold', 'italic', 'underline'],
        ['format-block'],
        ['font-size'],
        ['ordered-list', 'unordered-list', 'outdent', 'indent'],
        ['left-justify', 'center-justify', 'right-justify']
    ];
    $scope.cssClasses = ['test1', 'test2'];
    /*date*/
    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.clear = function () {
        $scope.dt = null;
    };
    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };
    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.maxDate = new Date(2020, 5, 22);
    $scope.open = function($event) {
        $scope.status.opened = true;
    };
    $scope.setDate = function(year, month, day) {
        $scope.dt = new Date(year, month, day);
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.status = {
        opened: false
    };
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 2);
    $scope.events =
        [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];
    $scope.getDayClass = function(date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i=0;i<$scope.events.length;i++){
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    };
    $scope.itemsperpage=12;
    $scope.bigCurrentPage = 1;
    $scope.initScope=1;
    $scope.bookFormat = ["jpeg", "jpg", "png", "gif", "mixedMedia", "PaperBack"];
    $scope.ispublishedPagination=0;
    $scope.isUnpublishedPagination=0;
    $scope.publishedPagination=0;
    $scope.unPublishedPagination=0;






    function init(){
        $scope.getAllBooks($scope.bigCurrentPage);
        $scope.getImagePathOfs3();
    }
    $scope.getImagePathOfs3=function(){
        bookService.getS3ImagePath().then(function(response){
            $scope.imagePathOfBook = response.data;
        });
    }
    $scope.getAllBooks = function(pageNo){
        bookService.setSelectedPageNo(pageNo);
        var pageNumber=bookService.getSelectedPageNo();
        $scope.currentPage = 1;
        var startPage=(pageNo-1)*$scope.itemsperpage;
        bookService.getBooksForPagination(startPage,$scope.itemsperpage).then(function(response){
            $scope.listingData = response.data;
            $scope.getTotalCountOfBook();
        });
    }
    $scope.getTotalCountOfBook = function(){
        bookService.getNumberOfBooks().then(function(response){
            $scope.bigTotalItems = response.data.NoOfBooks;
        });

    }


    $scope.clearSearchResult = function(pageNo){
        $scope.searchParameter=null;
        bookService.setSelectedPageNo(pageNo);
        var pageNumber=bookService.getSelectedPageNo();
        $scope.currentPage = 1;
        var startPage=(pageNo-1)*$scope.itemsperpage;
        bookService.getBooksForPagination(startPage,$scope.itemsperpage).then(function(response){
            $scope.listingData = response.data;
            $scope.getTotalCountOfBook();
        });
    }



    $scope.emptyFrom=function(){

        /*to avoid calling init methods*/
       /* $scope.initScope==0;*/
        $scope.book="";

    }
    $scope.addBook = function (book) {
        if (book != undefined && book.Title != undefined&&book.PublishedDate!=undefined&& book.ShortDesc != undefined&& book.LongDesc != undefined) {
            bookService.getNumberOfBooks().then(function (response) {
                $scope.totalNoOfBooks = response.data.NoOfBooks;
                book.bookId = $scope.totalNoOfBooks + 1;
                var id = book.bookId;
                book.sortId=book.bookId;
                $scope.master = angular.copy(book);
                $http.post('/api/addBook', $scope.master
                ).success(function (data) {
                        $location.path('/getAllBooksToCart');
                    }).error(function (err) {
                        alert(err);
                        console.log(err);
                    });

            })

        }
    };

    $scope.updateBookDetails = function (bookDetails) {
        if (bookDetails && bookDetails.Title  && bookDetails.PublishedDate && bookDetails.ShortDesc) {
            $scope.master = angular.copy(bookDetails);
            $http.post('/api/updateBookbyId', $scope.master).success(function (data) {
                $scope.openupdateSucessDialog(bookDetails);
                $scope.publish=1;
            }).error(function (err) {
                console.log(err);
            });
        }

    };
    $scope.openupdateSucessDialog = function (book) {
        $scope.book = book;
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/Success.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.openPublishBookCart = function (bookCartData) {
        $scope.bookCartDetails = bookCartData;
        if($scope.bookCartDetails.isPublished===0){
            console.log($scope.bookCartDetails.bookId);
            console.log($scope.bookCartDetails.isPublished);
            $scope.PublishStatusCart = "Publish the book and help the user to know about the book ";
        }else {
            $scope.PublishStatusCart = "By UnPublishing the book user will not be able to see it";
        }
        console.log("inside openPublishBook");
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/PublishUnpublishBookCart.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.publishBookCartDb = function (book) {
        var bookPublishId = book.bookId;
        var publishStatus = book.isPublished;
        if (publishStatus == 0) {
            bookService.publishBookById(bookPublishId).then(
                function (resultdata) {
                    console.log("***********************")
                    console.log($scope.isUnpublishedPagination)
                    if($scope.isUnpublishedPagination==1){
                        var currentpublishPage=bookService.getUnPublishedSelectedPageNo();
                        $scope.getUnpublishedBooks(currentpublishPage);

                    }
                    $modalStack.dismissAll();
                })
        }
        else {
            bookService.unPublishBookById(bookPublishId).then(
                function (resultdata) {
                    if($scope.ispublishedPagination==1){
                        var currentpublishPage=bookService.getPublishedSelectedPageNo();
                        $scope.getpublishedBooks(currentpublishPage);

                    }
                    $modalStack.dismissAll();
                })
        }

    };
    function changeDateFormat(date){
        var modifiedDate = date.substring(0, 10);
        return modifiedDate;
    }
    $scope.close = function () {
        $modalStack.dismissAll();
    };
    if($scope.initScope==1){
        init();
    }
    $scope.openDeleteBook = function (book) {
        $scope.book = book;
        /*$scope.isDeleteBook = isDeleteBook;*/
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/DeleteBook.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.deleteBookById = function (book) {
        var id = book.bookId;
        var deleteStatus = book.isDeleteBook;
        if (deleteStatus == 0) {
            bookService.unarchiveById(id).then(
                function (resultdata) {
                    var currentPage=bookService.getSelectedPageNo();
                    $scope.getAllBooks(currentPage);
                    if($scope.ispublishedPagination==1){
                        var currentpublishPage=bookService.getPublishedSelectedPageNo();
                        $scope.getpublishedBooks(currentpublishPage);

                    }
                    if($scope.isUnpublishedPagination==1){
                        var currentpublishPage=bookService.getUnPublishedSelectedPageNo();
                        $scope.getUnpublishedBooks(currentpublishPage);

                    }
                    $modalStack.dismissAll();
                })
        }
        else {
            bookService.deleteBookByIdDb(id).then(
                function (resultdata) {
                    var currentPage=bookService.getSelectedPageNo();
                    $scope.getAllBooks(currentPage);
                    if($scope.ispublishedPagination==1){
                        var currentpublishPage=bookService.getPublishedSelectedPageNo();
                        $scope.getpublishedBooks(currentpublishPage);

                    }
                    if($scope.isUnpublishedPagination==1){
                        var currentpublishPage=bookService.getUnPublishedSelectedPageNo();
                        $scope.getUnpublishedBooks(currentpublishPage);

                    }
                    $modalStack.dismissAll();
                })
        }

    };
    $scope.openPublishBook = function (book) {
        $scope.book = book;
        if(book && book.Title && book.ShortDesc){
            console.log(book.bookId);
            $scope.PublishStatus = "All the fields are provided book can be published";
        }else {
            $scope.PublishStatus = "All the fields are not provided book cannot be published";
        }
        console.log("inside openPublishBook");
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/PublishBook.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.publishBookDb = function (book) {
        var bookPublishId=book.bookId;
        bookService.publishBookById(bookPublishId).then(
            function (resultdata) {
                bookService.getBookByIdDb(bookPublishId).then(
                    function (resultdata) {
                        var bookDetailsDb = resultdata.data[0];
                        if(bookDetailsDb.PublishedDate!= undefined) {
                            var modifiedDate= changeDateFormat(bookDetailsDb.PublishedDate);
                            bookDetailsDb.PublishedDate=modifiedDate;
                            $scope.book = bookDetailsDb;
                            $modalStack.dismissAll();
                        }
                    })
            })
    }
    $scope.openUnPublishBook = function (book) {
        $scope.book = book;
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/UnPublish.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.UnpublishBookDb = function (book) {
        var bookunPublishId=book.bookId;
        bookService.unPublishBookById(bookunPublishId).then(
            function (resultdata) {
                console.log("published");
                bookService.getBookByIdDb(bookunPublishId).then(
                    function (resultdata) {
                        var bookDetailsDb = resultdata.data[0];
                        if(bookDetailsDb.PublishedDate!= undefined) {
                            var modifiedDate= changeDateFormat(bookDetailsDb.PublishedDate);
                            bookDetailsDb.PublishedDate=modifiedDate;
                            $scope.book = bookDetailsDb;
                            $modalStack.dismissAll();
                        }
                    })
            })

    }
    $scope.getBooksForSorting=function(){
        bookService.getBookTitleForSorting().then(function(response){
            $scope.sortBookTitle = response.data;

        });

    }
    $scope.getBooksForSorting();
    $scope.saveSorttedBooksTodb=function(sortBookTitle){

        var sortedBookArray=[];
        var l;
        for(l=0;l<sortBookTitle.length;l++){
            var obj={
                bookId:sortBookTitle[l].bookId,
                bookTitle:sortBookTitle[l].Title,
                sortId:l
            }
            sortedBookArray.push(obj);
            if(l==sortBookTitle.length-1){
                var masterobj={
                    sortedBookArray:sortedBookArray
                };

                bookService.storeSortedBookId(masterobj).then(function(response){
                    console.log("!!@#!@3")
                    $scope.openSortSuccessDialog();
                });


            }
        }




    }
    $scope.openSortSuccessDialog = function () {
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/successSort.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.getPublishedBookCount = function(){
        bookService.getTotalPublishedBookCount().then(function(response){
            $scope.totalPublishedBook = response.data.NoOfPublishedBooks;
        });
    }
    $scope.getpublishedBooks=function(pageNo){
        $scope.publishedPagination=1
        $scope.unPublishedPagination=0;
        $scope.ispublishedPagination=1;
        console.log(pageNo);
        bookService.setPublishedSelectedPageNo(pageNo);
        $scope.getPublishedBookCount();
        $scope.currentpublishedPage = 1;
        var startPage=(pageNo-1)*$scope.itemsperpage;
        bookService.getPublishedBook(startPage,$scope.itemsperpage).then(function(response){
            $scope.listingData = response.data;
        });
    }
    $scope.getUnpublishedBooks=function(pageNo){
        $scope.unPublishedPagination=1;
        $scope.publishedPagination=0;
        $scope.ispublishedPagination=1;
        console.log(pageNo);
        bookService.setUnPublishedSelectedPageNo(pageNo);
        $scope.getUnPublishedBookCount();
        $scope.currentUnpublishedPage = 1;
        var startPage=(pageNo-1)*$scope.itemsperpage;
        bookService.getUnPublishedBook(startPage,$scope.itemsperpage).then(function(response){
            $scope.listingData = response.data;
        });

    }
    $scope.getUnPublishedBookCount = function(){
        bookService.getTotalUnPublishedBookCount().then(function(response){
            $scope.totalUnPublishedBook = response.data.NoOfUnPublishedBooks;
        });
    }


/*serach*/
    $scope.locationURL="/search";
    $scope.searchBook = function(searchName) {
        $http.get("/search/book/name/"+searchName).then(function(response) {
            $scope.listingData = response.data;
            $scope.bigTotalItems=response.data.length;

            if($scope.ispublishedPagination==1){
                $scope.publishedBookArray = response.data;
                $scope.totalPublishedBook=response.data.length;
            }
            if($scope.isUnpublishedPagination==1){
                $scope.unPublishedBookArray = response.data;
                $scope.totalUnPublishedBook=response.data.length;

            }
        });
    };













    /*upload Images for books*/
    $scope.showImagestatus=1;
    $scope.showDataAddFile = function() {
        $scope.showData = true;
    };

    $scope.openDeleteImage = function (imageName,unqbookId) {
        $scope.imageName = imageName;
        $scope.unqbookId = unqbookId;
        console.log("inside openAddAuthor");
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/DeleteImage.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.deleteBookImage=function(imageName,bookId){
        bookService.deleteBookImageById(imageName,bookId).then(
            function (resultdata) {
                $modalStack.dismissAll();
            });

    }

    $scope.getImageForBookID=function(bookId){
        bookService.getImagesByBookID(bookId).then(function(response){
            if(response.data[0].Images){
                console.log(response.data[0].Images);
                $scope.imagesArray = response.data[0].Images;


            }

        });
    }
    $scope.uploadStatus=function(){
        $scope.imageUploadStatus="Image Uploaded Successfully"
    }

    var editBookId = $routeParams.editBookId;
    if(editBookId){
        $scope.uploadImageid = editBookId;
        uploadImageToDataBase($scope.uploadImageid);
        bookService.getBookByIdDb(editBookId).then(
            function (resultdata) {
                $scope.book = resultdata.data[0];
                var modifiedDate= changeDateFormat($scope.book.PublishedDate);
                $scope.book.PublishedDate=modifiedDate;
            })

    }


    function uploadImageToDataBase(uploadImageid){

        if (uploadImageid != undefined) {
            var updateImageByBookId = uploadImageid;
            var url = 'book/image/' + updateImageByBookId;
            $scope.options = {
                url: url,
                autoUpload: false,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i

            };



            $scope.getImageForBookID(uploadImageid);
        }
    }



            $scope.UpdateSelectName=function(dropDownType){
                $scope.selectedDropDownvalue=dropDownType;

            }





});