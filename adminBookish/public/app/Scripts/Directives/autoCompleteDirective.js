/**
 * Created by zendynamix on 05-03-2016.
 */
app.directive('autoCompleteDirective',function($http){
    var books = [];
    return{
        restrict:'A',
        scope:{
            url:'@'
        },
        link:function(scope,elm,attrs){
            elm.autocomplete({
                source:function(request,response){
                    $http({method:'get',url:scope.url,params:{search:request.term}}).success(function(data){
                        console.log("!!!!!!!!!!!!!!");
                        console.log(data);
                        books = [];
                        if(data[0]!=undefined &&data[0]. Title){
                            for(var k=0;k<data.length;k++){
                                console.log(data[k]. Title)
                                books.push(data[k]. Title)
                            }
                        }
                        else if(data[0]!=undefined &&data[0]. nickName){
                            for(var k=0;k<data.length;k++){
                                console.log(data[k]. nickName)
                                books.push(data[k]. nickName)
                            }
                        }else if(data[0]!=undefined &&data[0]. categoryName){
                            for(var k=0;k<data.length;k++){
                                console.log(data[k]. categoryName)
                                books.push(data[k]. categoryName)
                            }
                        }

                        response(books);
                    })
                },
                minLength:2
            })
        }
    }
})
