/**
 * Created by zendynamix on 12-05-2016.
 */
app.controller("categoryLabelModelController", function($scope,$http,$modal,dataService) {
    $scope.openAddCategoryLabel = function (categoryLabel) {
        $scope.message=null;
      /*  $scope.categoryData= angular.copy(categoryLabel);*/
        $scope.modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/addCategoryLabel.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.close=function(){
        $scope.modalInstance.dismiss();
    };
    $scope.addCategory = function (category) {
        if(category!=null){
            // check whether the operation is add or edit
                if (category.categoryName &&category.categoryDescription) {
                    findCategoryUniqueName(category);
                }


        }

    };
    function encodingFunction(uri) {
        var res = encodeURIComponent(uri);
        return res;
    }
    function findCategoryUniqueName(category){
        /*encoding inputs containing special character like / */
        var categoryInput=category.categoryName;
        var encodeInputData= encodingFunction(categoryInput);
        dataService.findCategoryByName(encodeInputData).then(
            function (payload) {
                $scope.categoryDataBase = payload.data[0];
                if (!$scope.categoryDataBase) {
                    addCategoryLabelFindId(category);

                } else {
                    $scope.message="category already exists";

                }
            })
    }
    function addCategoryLabelFindId(categoryDataFunctionInput){
        dataService.getMainCategoryCount().then(function (response) {
            var categoryLabelCount= response.data[0].count;
            categoryDataFunctionInput.categoryId = categoryLabelCount + 1;
            $http.post('/api/Category',categoryDataFunctionInput
            ).success(function () {
                    var pageNumber=dataService.getCurrentCategoryLabel();
                    $scope.close();
                    $scope.$emit('categoryLabelAdded', { currentPage: pageNumber });
                }).error(function () {
                    console.log("Error");
                });
        });

    }
    $scope.updateCategory = function (category) {
        $http.post('/api/Category/id',category
        ).success(function () {
                $scope.close();
                $scope.$emit('categoryLabelAdded', { currentPage: 1 });
            }).error(function (data) {
                console.log("Error");
            });

    };

})
app.directive("addCategoryLabel", function() {
    return {
        restrict: 'E',
        controller:"categoryLabelModelController",
        scope:{
            categoryData: '='
        },
        templateUrl: '../../view/templates/AddCategoryLabel.html'


    }
});