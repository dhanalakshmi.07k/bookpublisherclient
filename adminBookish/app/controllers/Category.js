/**
 * Created by zendynamix on 23-09-2015.
 */
/*var Promise = require('bluebird');*/
var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
var categoryAggregate = require('../aggregators');

module.exports = function (app) {
  app.use(router);
};
Categories= mongoose.model('Categories');

router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});

router.route('/api/SubCategory')
  .put(function(req,res){
      categoryAggregate.subCategoryDetails.addSubcategory(req,res);
  });

router.get('/Categories/subCategory/:page/:pageSize', function (req, res) {
  var page= req.params.page;
  var pageSize= req.params.pageSize;
  categoryAggregate.subCategoryDetails.getCategoryData(page,pageSize,res);
})


router.get('/Categories/test', function (req, res) {
    categoryAggregate.subCategoryDetails.showSubTestCategoryDetails(res);
})


router.get('/subCategory/count', function (req, res) {
  var page= req.params.page;
  var pageSize= req.params.pageSize;
  categoryAggregate.subCategoryDetails.getSubCategoryCount(res);
})


router.get('/subCategory/dropDown', function (req, res) {
    categoryAggregate.subCategoryDetails.subCategoryDropDown(res);
})

router.route('/api/update/SubCategoryDetails')
  .post(function(req,res){
    Categories.subCategory=[];
    Categories.findOne({ categoryName: req.body.categoryName}, function (err, catRes){
      catRes.subCategory = req.body.subCategory;
      catRes.save();
      res.send("successfuly added SubCategory")
    });
  })


router.route('/api/update/Category')
  .put(function(req,res){
    Categories.findOne({ categoryName: req.body.categoryName}, function (err, catRes){
      catRes.subCategory=req.body.subCategory;
      catRes.save();
      res.send("successfuly upadted SubCategory")
    });
  })

router.route('/update/subCategory')
    .post(function(req,res){
      var oldSubCategoryName=req.body. oldSubCategoryName;
      var categoryName=req.body.category;
      var subCategoryName=req.body.subCategoryName;
      var subCategoryDescription=req.body. subCategoryDescription;
      Categories.update(
          {"categoryName" : categoryName, "subCategory.name":oldSubCategoryName},
          { $set: { "subCategory.$.name" : subCategoryName,"subCategory.$.description" : subCategoryDescription} }, function (err, catRes){
            res.send("successfuly upadted SubCategory")
          })
    })


router.get('/update/Categories/subCategory/name/:oldCategory/:oldSubCategory', function (req, res) {
  var categoryName= req.params.oldCategory;
  var subCategoryName= req.params.oldSubCategory;
  Categories.update(
      {categoryName:categoryName },
      { $pull: { subCategory: { name:subCategoryName} } },
      { multi: false }, function (err, catRes){
        res.send("successfuly upadted SubCategory")
      })
})

router.get('/delete/subCategory/name/:categoryName/:subCategoryName', function (req, res) {
  var categoryName= req.params.categoryName;
  var subCategoryName= req.params.subCategoryName;
  Categories.update(
      {categoryName:categoryName },
      { $pull: { subCategory: { name:subCategoryName} } },
      { multi: false }, function (err, catRes){
        res.send("successfuly upadted SubCategory")
      })
})


