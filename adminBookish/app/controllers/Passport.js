/**
 * Created by zendynamix on 29-09-2015.
 */
var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var passportConfig=require('../../config/auth.js');
var passport=require('passport');
var FacebookStrategy  = require('passport-facebook').Strategy;

module.exports = function (app) {
  app.use(router);
};



Userpassport =  mongoose.model('User');

// test authentication
/*
 function ensureAuthenticated(req, res, next) {
 if (req.isAuthenticated()) { return next(); }
 res.redirect('/')
 }
 */

router.get('/test', function(req, res){
  res.render('auth.html');
});


router.get('/success', function(req, res){
  /*res.render({ url:"http://localhost:8000/#/"});*/
  /*res.redirect('http://localhost:8000/#/');*/
  res.render('success.html');
});


router.get(
  '/auth/facebook',
  passport.authenticate('facebook', { session: false, scope: [] })
);

router.get('/auth/facebook/callback',
  passport.authenticate('facebook', { session: false, failureRedirect: "/" }),
  function(req, res) {
    res.redirect("/success?access_token=" + req.user.access_token);
  }
);

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

// serialize and deserialize
passport.serializeUser(function(user, done) {
  console.log('serializeUser: ' + user._id)
  done(null, user._id);
});
passport.deserializeUser(function(id, done) {
  Userpassport.findById(id, function(err, user){
    console.log(user)
    if(!err) done(null, user);
    else done(err, null)
  })
});


// config
passport.use(new FacebookStrategy({
    clientID: passportConfig.facebook.clientID,
    clientSecret: passportConfig.facebook.clientSecret,
    callbackURL: passportConfig.facebook.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {
    console.log("!!!!!!!!!!!!!!!!");
    Userpassport.findOne({ oauthID: profile.id }, function(err, user) {
      if(err) { console.log(err); }
      if (!err && user != null) {
        done(null, user);
      } else {
        var user = new Userpassport({
          oauthID: profile.id,
          name: profile.displayName,
          access_token:accessToken,
          created: Date.now()
        });
        user.save(function(err) {
          if(err) {
            console.log(err);
          } else {
            console.log("saving user ...");
            done(null, user);
          };
        });
      };
    });
  }
));


