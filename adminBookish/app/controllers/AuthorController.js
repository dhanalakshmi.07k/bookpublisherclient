var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var logger = require('../../config/logger.js');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';

module.exports = function (app) {
  app.use(router);
};


Authors= mongoose.model('Authors');

router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});

router.route('/api/author')
  .get(function(req,res){
    var page= req.query.page;
    var pageSize= req.query.pageSize;
    console.log("page number is %s and page Size is %s", page,pageSize);
    if(!page || !pageSize) {
      res.status(500).send({error: 'Page=' + page + ' and/or pageSize=' + pageSize + ' is not valid'});
      return;
    }
    Authors.find(function(err,author){
      if(err){
        res.status(500).send(err.message);
        logger.info("unabel to get the records for the desired page and pagesize");
      }
      res.send(author);
      console.log("Get all the Authors");
    }).skip((page-1)*pageSize).limit(pageSize).sort({createdAt:-1});
  });


router.route('/api/author')
  .post(function(req,res){
    if(req.body._id!=undefined){
      console.log("inside edit");
      Authors.findOne({ _id: req.body._id}, function (err, authors){
        authors.authorId=req.body.authorId;
        authors.Description=req.body.Description;
        authors.isDeletedAuthor=1;
        authors.nickName=req.body.nickName;
        authors.name=req.body.name;
        authors.address=req.body.address;
        authors.save();
        res.send("author updated successfully")
      });
    }else{
      console.log("inside add");
      var authors = new Authors();
      console.log(req.body);
      authors.authorId=req.body.authorId;
      authors.Description=req.body.Description;
      authors.isDeletedAuthor=1;
      authors.nickName=req.body.nickName;
      authors.name=req.body.name;
      authors.address=req.body.address;
      authors.save(function(err,result){
        if(err){
          res.status(500).send(err.message);
          logger.info(err.message);
          console.log(err.stack);
        }else{
          res.send(result);

        }
      })
    }
  });



router.route('/api/authors/:id')
  .get(function(req,res){
    Authors.find({authorId : req.params.id},function(err,author){
      if(err){
        res.status(500).send(err.message);
        logger.info(err.message);
      }
      res.send(author);
      console.log("provide author by authorId");
    })
  });



router.route('/api/author/count')
  .get(function(req,res){
    Authors.count(function(err,author){
      if(err){
        res.status(500).send(err.message);
        logger.info(err.message);
      }
      var count = {NoOfAuthors: author};
      res.send(count);
    });
  });


router.route('/api/archive/author/:id')
  .post(function(req,res){
    console.log("delete authour"+req.params.id)
    Authors.findOne({_id: req.params.id}, function (err, updateRemove){
      updateRemove.isDeletedAuthor= 0;
      updateRemove.save();
      res.send(updateRemove)
    });
  });


router.route('/api/unArchive/author/:id')
  .post(function(req,res){
    console.log("delete authour"+req.params.id)
    Authors.findOne({_id: req.params.id}, function (err, updateRemove){
      updateRemove.isDeletedAuthor= 1;
      updateRemove.save();
      res.send(updateRemove)
    });
  });

router.route('/api/author/:nickName')
  .get(function(req,res){
    Authors.find({nickName : req.params.nickName},function(err,author){
      console.log( req.params.nickName);
      if(err){
        res.status(500).send(err.message);
        logger.info(err.message);
      }
      res.send(author);

      console.log("provide author by authorId");
    })
  });

router.route('/api/deleteAllAuthors')
  .get(function(req,res){
    // delete the book
    Authors.remove(function(err) {
      if (err){
        res.status(500).send(err.message);
        logger.info(err.message);
      }

      res.send("authors deleted")
      console.log('Authors deleted successfully');
    });
  });


router.route('/api/all/author')
  .get(function(req,res){
    Authors.find({},'name.firstName',function(err,author){
      res.send(author);
      console.log("Get author by limit");
    });
  });

router.route('/api/author/:start/:end')
  .get(function(req,res){
    Authors.find({},function(err,authors){
      if (err){
        res.status(500).send(err.message);
        logger.info(err.message);
      }
      res.send(authors);
    }).skip(req.params.start).limit(req.params.end).sort({ authorId: -1 });
  });



router.route('/api/findAllAuthors')
  .get(function(req,res){
    Authors.find(function(err,author){
      if (err){
        res.status(500).send(err.message);
        logger.info(err.message);
      }
      res.send(author);
      console.log("Get all the Authors");
    }).sort({createdAt:-1});
  });



router.route('/searchAuthor')
    .get(function(req,res){

      Authors.find({ $text : { $search : req.query.search }},{"_id":0,"nickName":1},function(err,authors){
        console.log("inside search");
        console.log(authors);
        res.send(authors);
      }).limit(5);
    })


router.route('/search/Author/name/:searchName')
    .get(function(req,res) {
      var searchParameter = "\"" + req.params.searchName + "\"";
      Authors.find({$text: {$search: searchParameter}}, function (err, books) {
        res.send(books);
      })
    })





router.route('/author/nickNames')
    .get(function(req,res){
      console.log("Get author by limit");
      Authors.find({},{_id:0,nickName:1},function(err,author){
        res.send(author);
      }).sort({authorId:-1});
    });


