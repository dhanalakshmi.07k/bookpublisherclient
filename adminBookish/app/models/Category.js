var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var bookCategorySchema = new mongoose.Schema(
  {
      categoryId: Number,
    categoryName : String,
    categoryDescription : String,
    isMainCategory : Number,
    subCategory : [ {
      name:String,
      description : String,
      categoryName : String,
      sortId:Number,
      createdOn:{type:Date,default:Date.now}
    }],
    createdOn:{type:Date,default:Date.now},
    updateOn:{type:Date,default:Date.now},
  },{collection: "Categories"});
bookCategorySchema.index({ categoryName: 'text'});
bookCategorySchema.pre("save",function(next){
  updateOn = Date.now();
  next();
})
  mongoose.model('Categories',bookCategorySchema);
