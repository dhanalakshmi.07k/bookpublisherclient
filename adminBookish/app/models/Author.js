var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var authorSchema = new mongoose.Schema(
  {
    /*_id:Number,*/
    authorId : Number,
    isDeletedAuthor:Number,
    name: {
      fullName: {
        firstName : String,
        lastName : String,
      }
    },
    nickName:{type:String,required:'Nick name is required', unique:true},
    address: {
      fullAddress: {
        city : String,
        state: String,
        Street: String,
        ZIpCode : Number,
        Country : String
      },
    },
    Description : String,
    createdAt:{type:Date,default:Date.now},
    updateAt:{type:Date,default:Date.now},
  },{collection: "Authors"});
  authorSchema.index({ nickName: 'text'});
  authorSchema.pre('update',function(){
    this.update({},{ $set: { updateAt: new Date() } });
   /* next();*/
  })

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('Authors',authorSchema);
