/**
 * Created by zendynamix on 24-05-2016.
 */
    var express = require('express');
    var mongoose = require('mongoose');
    mime = require('mime'),
    fs = require('fs'),
    AWS = require('aws-sdk'),
    router = express.Router();
    var cors = require('cors');
    var knox = require('knox');
    var s3CredencilasConfiguration=require('../../config/s3Credencials.js')
    var accessKeyId =  s3CredencilasConfiguration.credential.key;
    var secretAccessKey = s3CredencilasConfiguration.credential.secret;
    Books =  mongoose.model('Books');
    var config=require("../../config/config")
    // S3 Connector
    var connect = function() {
        return knox.createClient(s3CredencilasConfiguration.credential);
    };

        var saveImageToAwsS3=function(files,filePath){
            console.log("inside aws save")
            console.log(filePath)
            AWS.config.update({
                accessKeyId: accessKeyId,
                secretAccessKey: secretAccessKey
            });
            var s3 = new AWS.S3();
            var file = files;
                var path = filePath;
                fs.readFile(path, function(err, file_buffer){
                    var params = {
                        Bucket: config.imageBucketName,
                        Key: config.imageFolderName+file.name,
                        Body: file_buffer
                    };
                    s3.putObject(params, function (perr, pres) {
                        if (perr) {
                            console.log("Error uploading data: ", perr);
                        } else {
                            console.log("Successfully uploaded data to myBucket/myKey");
                        }
                    });
                });

        }
        var saveImageInS3=function(files,filePath){

    var client = connect();
    var file = files;
    var stream = fs.createReadStream(filePath)
    var mimetype = mime.lookup(filePath);
    var callbackResponse;

    if (mimetype.localeCompare('image/jpeg')
        || mimetype.localeCompare('image/pjpeg')
        || mimetype.localeCompare('image/png')
        || mimetype.localeCompare('image/gif')) {

        callbackResponse = client.putStream(stream, '/'+config.imageFolderName+file.name,
            {
                'Content-Type': mimetype,
                'Cache-Control': 'max-age=604800',
                'x-amz-acl': 'public-read',
                'Content-Length': file.size
            },
            function (err, result) {
                console.log("added to s3");
            }
        );
    } else {
        console.log("file type not specified")
    }

    callbackResponse.on('response', function (res) {
        console.log(res.statusCode)
        if (res.statusCode == 200) {
            console.log("success")
        } else {
            console.log(res.statusCode)

        }
    })
}
        var deleteImageToAwsS3=function(fileName){
            console.log("*******************************")
            console.log(fileName);
            console.log("*******************************");
            AWS.config.update({
                accessKeyId: accessKeyId,
                secretAccessKey: secretAccessKey
            });
            var s3 = new AWS.S3();
            var params = {
                Bucket: 'lucent-book-image',
                Key:'images/'+fileName
            };
            s3.deleteObject(params, function (perr, pres) {
                if (perr) {
                    console.log("Error uploading data: ", perr);

                } else {
                    console.log("Successfully deleted data to myBucket/myKey");

                }
            });

        }

module.exports={
    saveImageInS3:saveImageInS3,
    saveImageToAwsS3:saveImageToAwsS3,
    deleteImageToAwsS3:deleteImageToAwsS3

}